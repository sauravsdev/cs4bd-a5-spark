# CS4BD-A5-Spark
### Repository for the assignment A5-Spark  

**Course Name**: Computer Science for Big Data  
**Program**: Masters in Data Science  
**Semester**: Winter 2019-2020  
**Course Teacher**: Prof. Stefan Edlich  
**Contributor**: Saurav Kumar Saha  
#
#
#### Solutions:
**#1**  
[counting-shakespears-words.pdf](https://bitbucket.org/sauravsdev/cs4bd-a5-spark/src/master/counting-shakespears-words.pdf "counting-shakespears-words.pdf")
  
 
  
**#2**  
The resulting word (24th most occuring word): **'make'**
  

  
**#3**  
I have used [fscm/spark-jupyter](https://app.vagrantup.com/fscm/boxes/spark-jupyter "fscm/spark-jupyter") Vagrant Box to try out Spark in isolation from my Host OS.   
This Vagrant Box (Virtual Machine) comes with the following software setup -  
#
*  Spark: 2.2.0
*  Scala: 2.11.11
*  Python: 3.5.3
*  Jupyter Notebook: 5.0.0

**As we already learned about Vagrant from the same course earlier, I didn't face any mentionable critical problem.**  
  

  
#### Task Description

- (1) Load the complete Shakespeare writings, strip the header and search for the #24 most used word in his writings. Provide your code in one .pdf, .txt or by one link!

Additionally do the following:

- (2) What is the resulting word?
- (3) What installation did you work with? Were there any obstacles?
#
Ref to Shakespeares work: [Shakespeares work](https://ocw.mit.edu/ans7870/6/6.006/s08/lecturenotes/files/t8.shakespeare.txt)
#
**Deadline**: Monday, 10th February 2020, 12:00am 

#
#
#
#### Start/Stop **fscm/spark-jupyter** vagrant box using powershell
* Install VirtualBox and Vagrant
* Create a directory (e.g. fscm-spark-jupyter) with the Vagrantfile having the following specification - 
#
```Vagrantfile
	Vagrant.configure("2") do |config|
  		config.vm.box = "fscm/spark-jupyter"
  		config.vm.define "fscm-spark-jupyter"
  		config.vm.provider "virtualbox" do |vb|
    		vb.name = "fscm-spark-jupyter"
  		end
	end
```
#
* Start powershell and go to the vagrant-box directory (e.g. fscm-spark-jupyter) and use following command -  
	- vagrant up
* To stop the virtual machine - 
	- vagrant halt
#
#
#### Reference Links:
 - https://app.vagrantup.com/fscm/boxes/spark-jupyter
 - https://spark.apache.org
 - https://databricks-prod-cloudfront.cloud.databricks.com/public/4027ec902e239c93eaaa8714f173bcfc/2799933550853697/4425269662237297/2202577924924539/latest.html